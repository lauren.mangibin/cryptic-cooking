// NEW CODE FOR PROJECT
#include "dialogue.h"
using namespace godot;

Dialogue::Dialogue() {}

Dialogue::~Dialogue() {}

void Dialogue::_register_methods() {
	register_method("_ready", &Dialogue::_ready);
	register_method("_process", &Dialogue::_process);
	register_method("select_option", &Dialogue::select_option);
	register_method("select_option1", &Dialogue::select_option1);
	register_method("select_option2", &Dialogue::select_option2);
	register_method("select_option3", &Dialogue::select_option3);
	register_method("enter_scene", &Dialogue::enter_scene);
	register_method("leave_scene", &Dialogue::leave_scene);
	register_method("display_clue", &Dialogue::display_clue);
	register_method("hide_clue", &Dialogue::hide_clue);
	register_method("update_button", &Dialogue::update_button);
}

void Dialogue::_init() {
	// initialize any variables here
	ingredient = "";
	time_passed = 0.0;
	in_transition = false;
}

void Dialogue::_ready() {
	// Read json file
	File *file = File::_new();
	file->open("res://Assets/dialogue.json", File::READ);
	String jstring = file->get_as_text();
	file->close();

	// parse json into dictionary
	JSON *json_parser = JSON::get_singleton();
	Ref<JSONParseResult> json_res = json_parser->parse(jstring);
	dialogue = json_res->get_result();
	ingredient_clues = dialogue["ingredient_clues"];
	
	// Set up dialogue
	fullLabel = Object::cast_to<Label>(get_node("ColorRect/FullLabel"));
	halfLabel = Object::cast_to<Label>(get_node("ColorRect/HalfLabel"));
	button1Label = Object::cast_to<Label>(get_node("ColorRect/Button/Label"));
	button2Label = Object::cast_to<Label>(get_node("ColorRect/Button2/Label"));
	button3Label = Object::cast_to<Label>(get_node("ColorRect/Button3/Label"));
	
	button1 = Object::cast_to<Control>(get_node("ColorRect/Button"));
	button2 = Object::cast_to<Control>(get_node("ColorRect/Button2"));
	button3 = Object::cast_to<Control>(get_node("ColorRect/Button3"));

	button1->connect("pressed", this, "select_option1");
	button2->connect("pressed", this, "select_option2");
	button3->connect("pressed", this, "select_option3");

	main = Object::cast_to<Main>(get_node("/root/Main"));
	if (main == nullptr){
		Godot::print("!!! Dialogue has no Main");
		return;
	}	
	main->connect("_ingredient_clue_signal", this, "enter_scene");
	main->connect("_leave_scene_signal", this, "leave_scene");

	transition = Object::cast_to<Control>(get_node("/root/Main/Transition"));
	if (transition == nullptr) {
        Godot::print("!!! Dialogue has no transition");
		return;
    }
}

void Dialogue::_process(float delta) {
	// 3 sec wait period for transition
	if (in_transition) {
		time_passed += delta;
		if (time_passed > 3.0) {
			time_passed = 0.0;
			in_transition = false;
			display_clue();
		}
	}
}

void Dialogue::enter_scene(String _ingredient) {
	in_transition = true;
	ingredient = _ingredient;
	hide_clue();

	// play enter dialogue
	String scene = "store" + String::num_int64(main->num_store);
	Dictionary scene_dict = dialogue[scene];
	fullLabel->set_text(scene_dict["enter"]);
	transition->set_visible(true);
}

void Dialogue::leave_scene(String scene) {
	hide_clue();
	transition->set_visible(true);
	fullLabel->set_visible(false);
}

void Dialogue::display_clue() {
	// hide full label
	fullLabel->set_text("");
	Array specific_clues = ingredient_clues[ingredient];

	// add in random clues from dialogue
	int random_int1 = rand() % 2;
	halfLabel->set_text(specific_clues[random_int1]); 
	int random_int2 = rand() % 3;

	// make sure ingredient 3 is not repeated
	Array options = Array();
	Array ingredients = ingredient_clues.keys();
	int my_size = ingredients.size();

	for (int i = 0; i < 3; i++) {
		if (i == random_int2) {
			options.append(ingredient);
		} else {
			int random_int3 = rand() % my_size;
			// make sure options are unique
			while (ingredients[random_int3] == ingredient) { 
				random_int3 = rand() % my_size;
			}
			options.append(ingredients[random_int3]);
		}
	}

	update_button(button1, button1Label, options[0]);
	update_button(button2, button2Label, options[1]);
	update_button(button3, button3Label, options[2]);
}

void Dialogue::update_button(Control* button, Label* buttonLabel, String text) {
	button->set_visible(true);
	buttonLabel->set_text(text);
}

void Dialogue::hide_clue() {
	halfLabel->set_text("");
	button1->set_visible(false);
	button1Label->set_text("");
	button2->set_visible(false);
	button2Label->set_text("");
	button3->set_visible(false);
	button3Label->set_text("");

	transition->set_visible(false);
}

void Dialogue::select_option(Control* button, Label* label) {
	if (label->get_text() == ingredient) {
		fullLabel->set_text("Correct!");
	} else {
		fullLabel->set_text("Incorrect");
	}
	hide_clue();
}

void Dialogue::select_option1() {
	select_option(button1, button1Label);
}

void Dialogue::select_option2() {
	select_option(button2, button2Label);
}

void Dialogue::select_option3() {
	select_option(button3, button3Label);
}
