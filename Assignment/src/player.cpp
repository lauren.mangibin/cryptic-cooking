#include "player.h"
#include <String.hpp>

using namespace godot;

void Player::_register_methods() {
    register_method("_process", &Player::_process);
    register_method("_physics_process", &Player::_physics_process);
	register_method("_ready", &Player::_ready);

    // all of these need to be set in the EDITOR, NOT HERE
    // if you put a default value in here and don't override the default in godot, godot will treat it at negative four billion
    // if in the editor you do not see a circular arrow next to the property you should assume that it will not be set correctly
    register_property<Player, float>("walkSpeed",      &Player::walkSpeed,      0.0);
    register_property<Player, float>("acceleration",   &Player::acceleration,   0.0);
    register_property<Player, float>("jumpSpeed",      &Player::jumpSpeed,      0.0);
    register_property<Player, float>("gravity",        &Player::gravity,        0.0);
    register_property<Player, float>("maxFallSpeed",   &Player::maxFallSpeed,   0.0);
    register_property<Player, float>("glideFallSpeed", &Player::glideFallSpeed, 0.0);
    register_property<Player, float>("dashSpeed",      &Player::dashSpeed,      0.0);
    register_property<Player, float>("dashTime",       &Player::dashTime,       0.0);
    register_property<Player, float>("dashCooldown",   &Player::dashCooldown,   0.0);
    register_property<Player, float>("maxSlopeAngle",  &Player::maxSlopeAngle,  0.0);
    register_property<Player, float>("rotateSpeed",    &Player::rotateSpeed,    0.0);
    register_property<Player, float>("raycastOffset",  &Player::raycastOffset,  0.0);
}

Player::Player() {}
Player::~Player() {}

// please for the love of god do not put anything in this method
// i've had enough headaches already lol
void Player::_init() {}

void Player::_ready() {
    dashCurrentCooldown = 0;
    velocity = Vector3(0,0,0);

	stateMachine = Object::cast_to<StateMachine>(get_node("StateMachine"));
    if (stateMachine == nullptr){
        Godot::print("!!! player has no statemachine");
    }

    raycast = Object::cast_to<RayCast>(get_node("RayCast"));
    if (raycast == nullptr){
        Godot::print("!!! player has no raycast");
    }

    cameraController = Object::cast_to<CameraController>(get_node("SpringArm"));
    if (cameraController == nullptr){
        Godot::print("!!! player has no cameracontroller");
    }

    visuals = Object::cast_to<Spatial>(get_node("Visuals"));
    if (visuals == nullptr) {
        Godot::print("!!! player has no visuals");
    }

    hangAreaContainer = Object::cast_to<Spatial>(get_node("HangAreaRotate"));
    if (hangAreaContainer == nullptr) {
        Godot::print("!!! player has no hang area container");
    }

    jumpSound = Object::cast_to<AudioStreamPlayer3D>(get_node("JumpSound"));
    if (jumpSound == nullptr) {
        Godot::print("!!! player has no jump sound");
    }

    landSound = Object::cast_to<AudioStreamPlayer3D>(get_node("LandSound"));
    if (landSound == nullptr) {
        Godot::print("!!! player has no land sound");
    }

    dashSound = Object::cast_to<AudioStreamPlayer3D>(get_node("DashSound"));
    if (dashSound == nullptr) {
        Godot::print("!!! player has no dash sound");
    }
}

void Player::_process(float delta) {
    
}

void Player::_physics_process(float delta) {
    //stateMachine->update(delta);
    dashCurrentCooldown = Math::move_toward(dashCurrentCooldown, 0, delta);
    stateMachine->update(delta);
}


bool Player::canDash() {
    return hasDash && dashCurrentCooldown == 0;
}

void Player::hang(Vector3 displacement) {
    translate(displacement);
    stateMachine->_on_transition_to("HangState");
    
}

void Player::freeze(){
    stateMachine->_on_transition_to("FrozenState");
}