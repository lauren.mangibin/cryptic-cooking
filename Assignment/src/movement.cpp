#include "cameracontroller.h"
#include "movement.h"

#include <Godot.hpp>
#include <Object.hpp>
#include <Input.hpp>
#include <Node.hpp>

using namespace godot;

Movement::Movement() {}
Movement::~Movement() {}

void Movement::move(float delta, Player* player) {
    Vector3 newVelocity = player->move_and_slide(player->velocity, Vector3::UP, true, 4, player->maxSlopeAngle);
    
    // this will stop the player from sliding up along steep slopes
    if (newVelocity.y > player->velocity.y) {
        newVelocity.y = player->velocity.y;
    }
    player->velocity = newVelocity;
}

void Movement::rotate(float delta, Vector3 direction, Player* player, bool shouldSnapRotation) {
    if (direction.length() > 0.0001) {
        float rotateAngle = signed_angle_to(-player->visuals->get_global_transform().basis.z, direction.normalized(), Vector3::UP);
        if (!shouldSnapRotation) {
            rotateAngle = Math::clamp(rotateAngle, -player->rotateSpeed * delta, player->rotateSpeed * delta);
        }
        // fixes jitter
        if (abs(rotateAngle) > 0.05) {
            player->visuals->rotate(Vector3::UP, rotateAngle);
        }
    }

    Vector3 velocityH = player->velocity;
    velocityH.y = 0;
    if (velocityH.length() > 0.1) {
        float rotateAngle = signed_angle_to(player->hangAreaContainer->get_global_transform().basis.x, velocityH.normalized(), Vector3::UP);
        if (abs(rotateAngle) > 0.05) {
            player->hangAreaContainer->rotate(Vector3::UP, rotateAngle);
        }
    }
}

// found on https://www.reddit.com/r/godot/comments/kr583r/any_ideas_on_why_angle_to_is_always_positive_here/
float Movement::signed_angle_to(Vector3 u, Vector3 v, Vector3 axis) {
    if (abs(u.length()) < 0.001 || abs(v.length()) < 0.001) {
        return 0;
    }

    float dot_p = u.dot(v);
    float dir = u.cross(v).dot(axis);

    float unsignedAngle = acos(Math::clamp(dot_p / u.length() / v.length(), -0.999f, 0.999f));
    return dir > 0 ? unsignedAngle : -unsignedAngle;
}

Vector3 Movement::getInputDirection(CameraController* camera) {
    Input* input = Input::get_singleton();
    Vector3 inputDirection = Vector3();
    if (input->is_action_pressed("left")) {
        inputDirection.x -= 1;
    }

    if (input->is_action_pressed("right")) {
        inputDirection.x += 1;
    }

    if (input->is_action_pressed("forward")) {
        inputDirection.z -= 1;
    }

    if (input->is_action_pressed("back")) {
        inputDirection.z += 1;
    }

    float angle = camera->get_rotation().y;
    inputDirection = inputDirection.rotated(Vector3::UP, angle);

    return inputDirection;
}