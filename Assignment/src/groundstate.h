#pragma once

#include <Godot.hpp>
#include <Input.hpp>
#include "state.h"
#include "player.h"

namespace godot {
    class GroundState : public State {
        GODOT_CLASS(GroundState, State)

        private:
            Player* player; // set in enter()

        public:
            GroundState();
            ~GroundState();

            void _init();
            void enter(State* oldState);
            void exit(State* newState);
            void update(float delta) override;
            bool check_transitions();
    };
}