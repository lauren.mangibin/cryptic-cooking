// NEW CODE FOR PROJECT
#ifndef PARSER_H
#define PARSER_H

#include <Godot.hpp>
#include <Node.hpp>
#include <File.hpp>

namespace godot {

	class Parser : public Node {
		GODOT_CLASS(Parser, Node)

	private:
	public:

		Parser();

		~Parser();

		static void _register_methods();

		static void create_dictionary(Dictionary recipes, Array keys, String filepath);

	};

} // namespace godot

#endif
