// NEW CODE FOR PROJECT
#include "ingredientholder.h"

#include <string>
#include <vector>

using namespace godot;

void IngredientHolder::_register_methods() {
    register_method("on_IngredientHolder_body_entered", &IngredientHolder::on_IngredientHolder_body_entered);
    register_method("_ready", &IngredientHolder::_ready);
    register_signal<IngredientHolder>((char*)"_ingredient_signal", "ingredient_name", GODOT_VARIANT_TYPE_STRING);
}

IngredientHolder::IngredientHolder() {}

IngredientHolder::~IngredientHolder() {}

void IngredientHolder::_init() {
    // detect when something enters the ingredient area
    set_monitoring(true); 
}

void IngredientHolder::_ready() {
    collectSound = Object::cast_to<AudioStreamPlayer3D>(get_node("CollectSound"));
    if (collectSound == nullptr) {
        Godot::print("!!! ingredient has no collect sound");
        return;
    }

    // signal yourself when we enter something
    this->connect("body_entered", get_node(NodePath(String("../")+get_name())), "on_IngredientHolder_body_entered");
}

void IngredientHolder::on_IngredientHolder_body_entered(Node* body) {
    if (is_visible()) {
        set_visible(false);
        collectSound->play();
        emit_signal("_ingredient_signal", this->get_child(this->get_child_count()-1)->get_name());
    }
}

void IngredientHolder::set_nameTag(String name){
    nameTag = Object::cast_to<Label>(get_node("MeshInstance/Viewport/Label"));
    if (nameTag == nullptr) {
        Godot::print("!!! player has no name tag");
        return;
    }
    nameTag->set_text(name);
}