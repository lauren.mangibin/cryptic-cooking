// NEW CODE FOR PROJECT
#ifndef HUD_H
#define HUD_H

#include <Godot.hpp>
#include <Control.hpp>
#include <Label.hpp>

namespace godot {

class HUD : public Control {
    GODOT_CLASS(HUD, Control)

private:
    Label* you_win;
    Label* you_lose;
    Label* you_win_message;
    Label* you_lose_message;
    Label* restart;
	Label *recipe_list;

public:
    static void _register_methods();

    HUD();
    ~HUD();

    void _init();
    void _ready();
    void _process(float delta);

    void _display_message(Array recipe_indices);
};

}

#endif
