#include "state.h"

using namespace godot;


void State::_register_methods() {
    register_method("enter", &State::enter);
    register_method("exit", &State::exit);
    register_method("update", &State::update);
    register_method("check_transitions", &State::check_transitions);

    register_signal<State>((char*)"_transition_signal", "newStateName", GODOT_VARIANT_TYPE_STRING);
}

State::State() {}
State::~State() {}

void State::transition_to(String newStateName) {
    // emit signal syntax here
    emit_signal("_transition_signal", newStateName);
}

void State::enter(State* oldState) {}
void State::exit(State* newState) {}
void State::update(float delta) {}

bool State::check_transitions() {
    return false;
}



void State::_init() {

}