// NEW CODE FOR PROJECT
#include "parser.h"
using namespace godot;

Parser::Parser() {}

Parser::~Parser() {}

void Parser::_register_methods() {}

void Parser::create_dictionary(Dictionary recipes, Array keys, String file_path) {
	File *file = File::_new();
	file->open(file_path, File::READ);
	bool first_line = true;
	int counter = 0;

	if (file->is_open()) {
		while (!file->eof_reached()) {
			PoolStringArray content = file->get_csv_line();

			//get first line of labels
			if (first_line){
				for (int i = 0 ; i < content.size(); i++) {
					recipes[content[i]] = Array();
					keys.append(content[i]);
				}
				recipes["available"] = Array();
				first_line = false;
			}

			//get recipes
			else {
				//add recipe only if correct number of ingredients
				if(content.size() == keys.size()){
					for (int i = 0 ; i < content.size(); i++) {
						Array stuff = recipes[keys[i]];
						stuff.append(content[i]);

						recipes[keys[i]] = stuff;
					}

					//add position to index
					Array count = recipes["available"];
					count.append(counter);
					recipes["available"] = count;
					counter++;
				}
			}
		}
		// done parsing, now close file
		file->close();

	} else {
		Godot::print("!! Parser: file not opened");
		return;
	}

	/*** print dictionary ***
	for (int i = 0; i < keys.size(); i++){
		Godot::print(keys[i]);
		Godot::print(recipes[keys[i]]);
	}
	*/
}
