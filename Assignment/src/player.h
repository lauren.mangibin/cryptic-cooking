#ifndef PLAYER
#define PLAYER

#include <Godot.hpp>
#include <KinematicBody.hpp>
#include <RayCast.hpp>
#include <AudioStreamPlayer3D.hpp>
#include "statemachine.h"
#include "cameracontroller.h"

namespace godot {

    class Player : public KinematicBody {
        GODOT_CLASS(Player, KinematicBody)

    private:
        StateMachine* stateMachine;

    public:
        float walkSpeed;
        float acceleration;
        float jumpSpeed;
        float gravity;
        float maxFallSpeed;
        float glideFallSpeed;
        float dashSpeed;
        float dashTime;
        float dashCurrentCooldown;
        float dashCooldown;
        float maxSlopeAngle;
        float rotateSpeed;
        float raycastOffset;

        bool hasDash;
        
        Vector3 velocity;
        RayCast* raycast;
        CameraController* cameraController;
        Spatial* visuals;
        Spatial* hangAreaContainer;
        AudioStreamPlayer3D* jumpSound;
        AudioStreamPlayer3D* landSound;
        AudioStreamPlayer3D* dashSound;

        static void _register_methods();

        Player();
        ~Player();

        void _init(); // our initializer called by Godot

        void _ready();
        void _process(float delta);
        void _physics_process(float delta);

        bool canDash();
        void hang(Vector3 displacement);
        void freeze();
    };

}

#endif