#pragma once

#include <Godot.hpp>
#include <Node.hpp>
#include "state.h"
#include <unordered_map>
#include <string>

namespace godot {
    class StateMachine : public Node {
        GODOT_CLASS(StateMachine, Node)

        private:
            std::unordered_map<std::string, State*> states;
            State* currentState;

        public:
            static void _register_methods();

            StateMachine();
            ~StateMachine();

            void _init();

            void _ready();
            void update(float delta);
            void _on_transition_to(String newStateString);
    };
}