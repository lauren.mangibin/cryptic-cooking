#include "groundstate.h"
#include "cameracontroller.h"
#include "movement.h"
#include <Input.hpp>

using namespace godot;

GroundState::GroundState() {}
GroundState::~GroundState() {}

void GroundState::_init() {

}

void GroundState::enter(State* oldState) {
    player = Object::cast_to<Player>(get_owner());
    player->hasDash = true;
}

void GroundState::exit(State* newState) {

}

void GroundState::update(float delta) {
    Vector3 inputDirection = Movement::getInputDirection(player->cameraController);
    Movement::rotate(delta, inputDirection, player, false);

    inputDirection.y = player->velocity.y;

    player->velocity = player->velocity.move_toward(inputDirection * player->walkSpeed, player->acceleration);

    Vector3 scaledVelocity = Vector3(player->velocity.x, 0, player->velocity.z).normalized() * player->raycastOffset;

    if (scaledVelocity != Vector3::ZERO) {
        player->raycast->set_translation(scaledVelocity);
    }
    player->velocity.y = -player->gravity * delta;
    
    Input* input = Input::get_singleton();
    if (!player->raycast->is_colliding() && input->is_action_pressed("sneak")) {
        player->velocity.x = 0;
        player->velocity.z = 0;
    }

    player->velocity = player->move_and_slide(player->velocity, Vector3::UP, true, 4, player->maxSlopeAngle);
}

bool GroundState::check_transitions() {
    //emit transition signal when we see a transition we want to make
    Input* input = Input::get_singleton();
    if (!player->is_on_floor()) {
       emit_signal("_transition_signal", "AirState");
       return true; 
    }

    if (input->is_action_pressed("jump")) {
       emit_signal("_transition_signal", "JumpState");
       return true; 
    }

    if (input->is_action_pressed("dash") && player->canDash() && Movement::getInputDirection(player->cameraController).length() > 0) {
       emit_signal("_transition_signal", "DashState");
       return true;
    }
    return false;
}
