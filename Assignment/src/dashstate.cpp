#include "dashstate.h"
#include <Input.hpp>

using namespace godot;

DashState::DashState() {}
DashState::~DashState() {}

void DashState::_init() {

}

void DashState::enter(State* oldState) {
    player = Object::cast_to<Player>(get_owner());
    player->hasDash = false;
    
    timePassed = 0;

    dashDirection = Movement::getInputDirection(player-> cameraController);
    Movement::rotate(0.1, dashDirection, player, true);

    // play sound
    player->dashSound->play();
}

void DashState::exit(State* newState) {
    player->dashCurrentCooldown = player->dashCooldown;
    player->velocity = player->velocity.normalized() * player->walkSpeed;
}

void DashState::update(float delta) {
    timePassed += delta;

    player->velocity = dashDirection * player->dashSpeed;
    Movement::move(delta, player);
}

bool DashState::check_transitions() {
    if (timePassed >= player->dashTime) {
        if (player->is_on_floor()) {
            emit_signal("_transition_signal", "GroundState");
        } else {
            emit_signal("_transition_signal", "AirState");
        }
        return true; 
    }

    return false;
}
