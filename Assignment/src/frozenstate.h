#pragma once

#include <Godot.hpp>
#include <Input.hpp>
#include "state.h"
#include "player.h"

namespace godot {
    class FrozenState : public State {
        GODOT_CLASS(FrozenState, State)

        private:
            Player* player; // set in enter()

        public:
            FrozenState();
            ~FrozenState();

            void _init();
            void enter(State* oldState);
            void exit(State* newState);
            void update(float delta) override;
            bool check_transitions();
    };
}