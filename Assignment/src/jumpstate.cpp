#include "jumpstate.h"
#include <Input.hpp>
#include "cameracontroller.h"
#include "movement.h"

using namespace godot;

JumpState::JumpState() {}
JumpState::~JumpState() {}

void JumpState::_init() {

}

void JumpState::enter(State* oldState) {
    player = Object::cast_to<Player>(get_owner());

    // set direction up
    player->velocity.y =  player->jumpSpeed;

    // play sound
    player->jumpSound->play();
}

void JumpState::exit(State* newState) {

}

void JumpState::update(float delta) {
    Vector3 inputDirection = Movement::getInputDirection(player->cameraController);
    Movement::rotate(delta, inputDirection, player, false);

    inputDirection *= player->walkSpeed;
    inputDirection.y = player->velocity.y;
    player->velocity = player->velocity.move_toward(inputDirection, player->acceleration);
    
    // gravity
    player->velocity.y = Math::move_toward(player->velocity.y, -player->maxFallSpeed, player->gravity * delta);
    Movement::move(delta, player);
    
}

bool JumpState::check_transitions() {
    //emit transition signal when we see a transition we want
    Input* input = Input::get_singleton();

    // release jump button
    if (!input->is_action_pressed("jump")) {
        player->velocity.y /= 2;
        emit_signal("_transition_signal", "AirState");
        return true; 
    }

    if (input->is_action_pressed("dash") && player->canDash() && Movement::getInputDirection(player->cameraController).length() > 0) {
       emit_signal("_transition_signal", "DashState");
       return true;
    }

    // start falling
    if (player->velocity.y <= 0) {
       emit_signal("_transition_signal", "AirState");
       return true; 
    }
    return false;
}
