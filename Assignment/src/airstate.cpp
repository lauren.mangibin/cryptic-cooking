#include "airstate.h"
#include "cameracontroller.h"
#include "movement.h"
#include <Input.hpp>
#include <Math.hpp>

using namespace godot;

AirState::AirState() {}
AirState::~AirState() {}

void AirState::_init() {

}

void AirState::enter(State* oldState) {
    player = Object::cast_to<Player>(get_owner());
}

void AirState::exit(State* newState) {
    if (newState->get_name() == "GroundState") {
        // play sound
        player->landSound->play();
    }
}

void AirState::update(float delta) {
    Vector3 inputDirection = Movement::getInputDirection(player->cameraController);
    Movement::rotate(delta, inputDirection, player, false);
    inputDirection.y = player->velocity.y;

    // inputDirection.y = player->velocity.y;
    // player->velocity = player->velocity.move_toward(inputDirection * player->walkSpeed, player->acceleration);

    inputDirection *= player->walkSpeed;
    inputDirection.y = player->velocity.y;
    player->velocity = player->velocity.move_toward(inputDirection, player->acceleration);
    
    // gravity
    player->velocity.y = Math::move_toward(player->velocity.y, -player->maxFallSpeed, player->gravity * delta);
    Movement::move(delta, player);
    
}

bool AirState::check_transitions() {
    Input* input = Input::get_singleton();

    //emit transition signal when we see a transition we want to make?
    if (player->is_on_floor()) {
       emit_signal("_transition_signal", "GroundState");
       return true; 
    }

    if (input->is_action_pressed("dash") && player->canDash() && Movement::getInputDirection(player->cameraController).length() > 0) {
       emit_signal("_transition_signal", "DashState");
       return true;
    }

    // press jump button
    if (input->is_action_pressed("jump")) {
        emit_signal("_transition_signal", "GlideState");
        return true; 
    }

    return false;
}
