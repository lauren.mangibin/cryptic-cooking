#include "audiotoggle.h"
#include <AudioServer.hpp>

using namespace godot;

AudioToggle::AudioToggle() {}
AudioToggle::~AudioToggle() {}

void AudioToggle::_register_methods() {
    register_method("_ready", &AudioToggle::_ready);
    register_method("_on_toggled", &AudioToggle::_on_toggled);
    register_property<AudioToggle, String>("targetBus", &AudioToggle::targetBus, "");
}

void AudioToggle::_init() {}

void AudioToggle::_ready() {
    this->connect("toggled", get_node(NodePath(String("../")+get_name())), "_on_toggled");
}

void AudioToggle::_on_toggled(bool isOn) {
    Godot::print(targetBus + String(" ") + String::num_real(AudioServer::get_singleton()->get_bus_count()));
    AudioServer::get_singleton()->set_bus_mute(
        AudioServer::get_singleton()->get_bus_index(targetBus), !isOn
    );
}