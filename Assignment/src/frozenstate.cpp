#include "frozenstate.h"
#include <Input.hpp>
#include <SceneTree.hpp>

using namespace godot;

FrozenState::FrozenState() {}
FrozenState::~FrozenState() {}

void FrozenState::_init() {
}

void FrozenState::enter(State* oldState) {
}

void FrozenState::exit(State* newState) {
}

void FrozenState::update(float delta) {
    Input* input = Input::get_singleton();
    if (input->is_action_pressed("restart")) {
        get_tree()->reload_current_scene();
    }
}

bool FrozenState::check_transitions() {
    return false;
}
