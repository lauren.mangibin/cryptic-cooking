#include "cameracontroller.h"
#include <Input.hpp>
#include <Math.hpp>

using namespace godot;

void CameraController::_register_methods() {
    register_method("_physics_process", &CameraController::_physics_process);
    register_property<CameraController, float>("panSpeed", &CameraController::panSpeed, 0.1f);
    register_property<CameraController, float>("panMaxSpeed", &CameraController::panMaxSpeed, 0.1f);
    register_property<CameraController, float>("panAcceleration", &CameraController::panAcceleration, 0.01f);
}

CameraController::CameraController() {
}

CameraController::~CameraController() {
}

void CameraController::_init() {
    panMaxSpeed = 0.1f;
    panSpeed = 0.0f;
    panAcceleration = 0.01f;
}

void CameraController::_physics_process(float delta) {
    Input* input = Input::get_singleton();
    int inputDirection = 0;
    if (input->is_action_pressed("pan_right")) {
        inputDirection -= 1;
    }
    
    if (input->is_action_pressed("pan_left")) {
        inputDirection += 1;
    }

    panSpeed = Math::move_toward(panSpeed, inputDirection * panMaxSpeed, panAcceleration * delta);
    rotate_y(panSpeed);
}

