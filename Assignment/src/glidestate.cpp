#include "glidestate.h"
#include <Input.hpp>
#include "movement.h"
#include "cameracontroller.h"

using namespace godot;

GlideState::GlideState() {}
GlideState::~GlideState() {}

void GlideState::_init() {

}

void GlideState::enter(State* oldState) {
    player = Object::cast_to<Player>(get_owner());

    // set direction up
    // UPDATE PARAMETERS
    if(player->velocity.y < -player->glideFallSpeed)
    {
        player->velocity.y = -player->glideFallSpeed;
    }
}

void GlideState::exit(State* newState) {

}

void GlideState::update(float delta) {
    Vector3 inputDirection = Movement::getInputDirection(player->cameraController);
    Movement::rotate(delta, inputDirection, player, false);

    inputDirection *= player->walkSpeed;
    inputDirection.y = player->velocity.y;
    player->velocity = player->velocity.move_toward(inputDirection, player->acceleration);
    
    // gravity;
    player->velocity.y = Math::move_toward(player->velocity.y, -player->glideFallSpeed, player->gravity * delta);
    Movement::move(delta, player);
       
}

bool GlideState::check_transitions() {
    //emit transition signal when we see a transition we want
    Input* input = Input::get_singleton();

    if (player->is_on_floor()) {
       emit_signal("_transition_signal", "GroundState");
       return true; 
    }

    if (input->is_action_pressed("dash") && player->canDash() && Movement::getInputDirection(player->cameraController).length() > 0) {
       emit_signal("_transition_signal", "DashState");
       return true;
    }

    // release jump button
    if (!input->is_action_pressed("jump")) {
        emit_signal("_transition_signal", "AirState");
        return true; 
    }
    return false;
}