#include "hangstate.h"
#include "cameracontroller.h"
#include "movement.h"
#include <Input.hpp>

using namespace godot;

HangState::HangState() {}
HangState::~HangState() {}

void HangState::_init() {

}

void HangState::enter(State* oldState) {
    player = Object::cast_to<Player>(get_owner());
    if (player == nullptr){
        Godot::print("!!! player null");
        return;
    }

    player->velocity = Vector3::ZERO;
}

void HangState::exit(State* newState) {
}

void HangState::update(float delta) {
}

bool HangState::check_transitions() {
    //emit transition signal when we see a transition we want to make
    Input* input = Input::get_singleton();
    if (input->is_action_pressed("jump")) {
        transition_to("JumpState");
        return true;
    }

    if (input->is_action_pressed("sneak")) {
        transition_to("AirState");
        return true;
    }

    return false;
}
