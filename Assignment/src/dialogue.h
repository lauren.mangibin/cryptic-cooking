// NEW CODE FOR PROJECT
#ifndef DIALOGUE_H
#define DIALOGUE_H

#include "main.h"

#include <File.hpp>
#include <Godot.hpp>
#include <ColorRect.hpp>
#include <JSON.hpp>
#include <JSONParseResult.hpp>
#include <Control.hpp>
#include <Label.hpp>

namespace godot {

class Dialogue : public ColorRect {
	GODOT_CLASS(Dialogue, ColorRect)

	private:

		Control *button1;
		Control *button2;
		Control *button3;
		Dictionary dialogue;
		Dictionary ingredient_clues;
		Label *fullLabel;
		Label *halfLabel;
		Label *button1Label;
		Label *button2Label;
		Label *button3Label;
		String ingredient;

		Main* main;
		Control *transition;
		float time_passed;
		bool in_transition;

	public:

		Dialogue();

		~Dialogue();

		static void _register_methods();

		void _init();
		void _ready();
		void _process(float delta);

		void enter_scene(String _ingredient);
		void leave_scene(String scene);
		void display_clue();

		void update_button(Control *button, Label *buttonLabel, String text);
		void hide_clue();
		void select_option(Control *button, Label* label);
		void select_option1();
		void select_option2();
		void select_option3();
	};

} // namespace godot

#endif
