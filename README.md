# Cryptic Cooking
## Build Steps:
```
git clone https://gitlab.com/lauren.mangibin/cryptic-cooking.git
cd cryptic-cooking
git submodule update --remote --init --recursive
cd Assignment/godot-cpp/godot-headers
git checkout 3.3
cd ..
git checkout 3.3
scons platform=<platform> generate_bindings=yes -j8
cd .. 
scons platform=<platform>
```

##Assets:
###food: https://vikingtabern.itch.io/3d-foods-for-games   
###terrain: https://fertile-soil-productions.itch.io/modular-terrain-pack   
###storage: https://brokenvector.itch.io/low-poly-storage-pack
###cow: https://quaternius.itch.io/lowpoly-animated-animals
