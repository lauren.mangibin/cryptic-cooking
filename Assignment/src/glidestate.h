#pragma once

#include <Godot.hpp>
#include <Input.hpp>
#include "state.h"
#include "player.h"

namespace godot {
    class GlideState : public State {
        GODOT_CLASS(GlideState, State)

        private:
            Player* player; // set in enter()

        public:
            GlideState();
            ~GlideState();

            void _init();
            void enter(State* oldState);
            void exit(State* newState);
            void update(float delta) override;
            bool check_transitions();
    };
}