#pragma once

#include <Godot.hpp>
#include <Input.hpp>
#include "state.h"
#include "player.h"

namespace godot {
    class HangState : public State {
        GODOT_CLASS(HangState, State)

        private:
            Player* player; // set in enter()

        public:
            HangState();
            ~HangState();

            void _init();
            void enter(State* oldState);
            void exit(State* newState);
            void update(float delta) override;
            bool check_transitions();
    };
}