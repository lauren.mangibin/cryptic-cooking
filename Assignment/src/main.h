// NEW CODE FOR PROJECT
#ifndef MAIN_H
#define MAIN_H

#include "hud.h"
#include "intro.h"
#include "parser.h"

#include <Godot.hpp>
#include <Spatial.hpp>
#include <File.hpp>
#include <time.h>

namespace godot {

class Main : public Spatial {
	GODOT_CLASS(Main, Spatial)

private:
	HUD* hud;
	Control* transition;
	Intro* intro;

	int recipe_index; // index of given recipe
	bool end_of_game;

public:
	
	Dictionary recipes; // recipe and ingredients
	Array keys;
	Array recipe_indices; // indices in available list
	int num_store; // current store we're in

	Main();

	~Main();

	static void _register_methods();

	void _init(); // our initializer called by Godot
	void _ready();
	void _process(float delta);

	void intro_scene();
	void end_scene();
	void transition_scene();
	
	void add_scene();
	void pick_recipe();
	void hide_ingredients();

	void _ingredient_collision(String ingredient);
	void update_recipes(String ingredient);

	void next_store();

	void print_dictionary();

};

} // namespace godot

#endif
