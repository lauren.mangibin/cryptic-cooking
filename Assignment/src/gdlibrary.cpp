#include <Godot.hpp>

//player
#include "player.h"
#include "cameracontroller.h"
#include "state.h"
#include "statemachine.h"
#include "groundstate.h"
#include "airstate.h"
#include "jumpstate.h"
#include "glidestate.h"
#include "dashstate.h"
#include "hangstate.h"
#include "frozenstate.h"
#include "audiotoggle.h"

// new code
#include "main.h"
#include "parser.h"
#include "ingredientholder.h"
#include "dialogue.h"
#include "hud.h"
#include "intro.h"
#include "cow.h"

extern "C" void GDN_EXPORT godot_gdnative_init(godot_gdnative_init_options *o) {
	godot::Godot::gdnative_init(o);
}

extern "C" void GDN_EXPORT godot_gdnative_terminate(godot_gdnative_terminate_options *o) {
	godot::Godot::gdnative_terminate(o);
}

extern "C" void GDN_EXPORT godot_nativescript_init(void *handle) {
	godot::Godot::nativescript_init(handle);

	//Player (Reused code)
	godot::register_class<godot::Player>();
    godot::register_class<godot::State>();
    godot::register_class<godot::StateMachine>();
    godot::register_class<godot::GroundState>();
    godot::register_class<godot::AirState>();
    godot::register_class<godot::JumpState>();
    godot::register_class<godot::GlideState>();
    godot::register_class<godot::DashState>();
    godot::register_class<godot::HangState>();
    godot::register_class<godot::CameraController>();
    godot::register_class<godot::FrozenState>();
    godot::register_class<godot::AudioToggle>();
    
    // NEW CODE FOR PROJECT
    godot::register_class<godot::Main>();
	godot::register_class<godot::Parser>();
    godot::register_class<godot::IngredientHolder>();
	godot::register_class<godot::Dialogue>();
	godot::register_class<godot::HUD>();
	godot::register_class<godot::Intro>();

    // Reused some AI code
	godot::register_class<godot::Cow>();

}
