// NEW CODE FOR PROJECT
#include "intro.h"

using namespace godot;

void Intro::_register_methods() {
	register_method("_display_message", &Intro::_display_message);
	register_method("_ready", &Intro::_ready);
}

Intro::Intro() {}

Intro::~Intro() {}

void Intro::_init() {}

void Intro::_ready() {
	recipe = Object::cast_to<Label>(get_node("Recipe"));
	if (recipe == nullptr) {
		Godot::print("!!! Intro has no recipe label");
		return;
	}
}

void Intro::_display_message(String recipe_name) {
	recipe->set_text(recipe_name);
}
