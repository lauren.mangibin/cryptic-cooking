#include "statemachine.h"
#include <future>

using namespace godot;

void StateMachine::_register_methods() {
    register_method("_ready", &StateMachine::_ready);
    register_method("update", &StateMachine::update);
    register_method("_on_transition_to", &StateMachine::_on_transition_to);
}

void StateMachine::_ready() {
    auto children = get_children();
    for (int i = 0; i < children.size(); i++) {
        State* state = Object::cast_to<State>(children[i]);
        if (state != nullptr) {
            states[state->get_name().utf8().get_data()] = state;
            state->connect("_transition_signal", this, "_on_transition_to");

            if (currentState == nullptr)
            {
                currentState = state;
                currentState->enter(nullptr);
            }
        }
    }
}

void StateMachine::update(float delta) {
    if (currentState == nullptr)
    {
        Godot::print("!!! no currentState");
        return;
    }

    int maxTransitions = 10;
    while (currentState->check_transitions() && maxTransitions > 0) {
        maxTransitions--;
    }
    
    currentState->update(delta);
}

void StateMachine::_on_transition_to(String newStateString) {
    std::string newStateStr = newStateString.utf8().get_data();

    if (states.count(newStateStr) == 0) {
        Godot::print("!!! state " + newStateString + " does not exist");
        return;
    }

    State* oldState = currentState;
    State* newState = states[newStateStr];

    oldState->exit(newState);
    currentState = newState;
    newState->enter(oldState);
}

StateMachine::StateMachine() {}
StateMachine::~StateMachine() {}

void StateMachine::_init() {
    states = std::unordered_map<std::string, State*>();
    currentState = nullptr;
}