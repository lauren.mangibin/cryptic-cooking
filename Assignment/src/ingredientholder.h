// NEW CODE FOR PROJECT
#ifndef INGREDIENTHOLDER_H
#define INGREDIENTHOLDER_H

#include <Godot.hpp>
#include <Area.hpp>
#include <AudioStreamPlayer3D.hpp>
#include <Label.hpp>

namespace godot {

class IngredientHolder : public Area {
    GODOT_CLASS(IngredientHolder, Area)

    private:
        AudioStreamPlayer3D* collectSound;
        Label* nameTag;

    public:
        static void _register_methods();

        IngredientHolder();
        ~IngredientHolder();

        void _init();
        void _ready();
        void _process(float delta);

        void on_IngredientHolder_body_entered(Node* body);
        void set_nameTag(String name);
    };

}

#endif