#pragma once

#include <Godot.hpp>
#include <Input.hpp>
#include "state.h"
#include "player.h"
#include "movement.h"

namespace godot {
    class DashState : public State {
        GODOT_CLASS(DashState, State)

        private:
            Player* player; // set in enter()
            float timePassed;

            Vector3 dashDirection;

        public:
            DashState();
            ~DashState();

            void _init();
            void enter(State* oldState);
            void exit(State* newState);
            void update(float delta) override;
            bool check_transitions();
    };
}