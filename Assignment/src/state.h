#pragma once

#include <Godot.hpp>
#include <Node.hpp>

namespace godot {
    class State : public Node {
        GODOT_CLASS(State, Node)

        private:

        public:
            static void _register_methods();

            State();
            ~State();

            void _init();

            virtual void enter(State* oldState);
            virtual void exit(State* newState);
            virtual void update(float delta);
            virtual bool check_transitions();
            virtual void transition_to(String newStateName);
    };
}