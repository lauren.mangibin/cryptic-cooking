// NEW CODE FOR PROJECT
#include "hud.h"

using namespace godot;

void HUD::_register_methods() {
    register_method("_display_message", &HUD::_display_message);
    register_method("_ready", &HUD::_ready);
}

HUD::HUD() {}

HUD::~HUD() {}

void HUD::_init() {}

void HUD::_ready() {
    you_win = Object::cast_to<Label>(get_node("YouWin"));
    if (you_win == nullptr) {
        Godot::print("!!! HUD has no win label");
    }

    you_win_message = Object::cast_to<Label>(get_node("WinMessage"));
    if (you_win_message == nullptr) {
        Godot::print("!!! HUD has no win message label");
    }

    you_lose = Object::cast_to<Label>(get_node("YouLose"));
    if (you_lose == nullptr) {
        Godot::print("!!! HUD has no lose label");
    }

    you_lose_message = Object::cast_to<Label>(get_node("LoseMessage"));
    if (you_lose_message == nullptr) {
        Godot::print("!!! HUD has no lose  message label");
    }

    restart = Object::cast_to<Label>(get_node("Restart"));
    if (restart == nullptr) {
        Godot::print("!!! HUD has no restart label");
    }

	recipe_list = Object::cast_to<Label>(get_node("RecipeList"));
	if (recipe_list == nullptr) {
		Godot::print("!!! HUD has no recipe list label");
	}

    you_win->set_visible(false);
    you_win_message->set_visible(false);
    you_lose->set_visible(false);
    you_lose_message->set_visible(false);
    restart->set_visible(false);
	recipe_list->set_visible(false);

}

void HUD::_display_message(Array recipes) {
	if (recipes.size() == 1) { // win
        you_win->set_visible(true);
        you_win_message->set_visible(true);
    } else {
        you_lose->set_visible(true);
        you_lose_message->set_visible(true);
    }
    restart->set_visible(true);
	recipe_list->set_visible(true);
	String recipe_list_str = "The goal recipe was ";
	for (int i = 0; i < recipes.size(); i++) {
		String recipe = recipes[i];
		if (i == 0) {
			recipe_list_str = recipe_list_str + recipe ;
		} else if (i < recipes.size() - 1) {
			recipe_list_str = recipe_list_str + " we also attempted " + recipe + ",";
		} else {
			recipe_list_str = recipe_list_str + " and we ended up with " + recipe;
		}
			
	}
	recipe_list->set_text(recipe_list_str);
}
