// NEW CODE FOR PROJECT
#ifndef COW_H
#define COW_H

#include "player.h"

#include <Godot.hpp>
#include <KinematicBody.hpp>
#include <Navigation.hpp>
#include <Area.hpp>

//Class for Guard AI

namespace godot {

class Cow : public KinematicBody {
	GODOT_CLASS(Cow, KinematicBody)

private:
	PoolVector3Array path;
	int64_t path_node;
	int64_t speed;
	int64_t state;
	Navigation *nav;
	Player *player;
	Spatial *milk;
	int64_t radius;

	//STATES
	int64_t REST = 0;
	int64_t ORBIT = 1;
	int64_t BLOCK = 2;

public:
	static void _register_methods();
	void _init();
	void _ready();
	void _physics_process(float delta);
	void move_to_block();
	void calc_block();
	void scan_for_player(Node *obj);
	void orbit();
	void block_path(Vector3 goal);
	void create_circle(Vector3 center);
	void timer_timeout();
	bool is_milk();
};

} // namespace godot

#endif //COW_H
