// NEW CODE FOR PROJECT
//created by marika
#include "cow.h"

using namespace godot;

void Cow::_register_methods() {
	register_method("_ready", &Cow::_ready);
	register_method("_physics_process", &Cow::_physics_process);
	register_method("move_to_block", &Cow::move_to_block);
	register_method("calc_block", &Cow::calc_block);
	register_method("scan_for_player", &Cow::scan_for_player);
	register_method("block_path", &Cow::block_path);
	register_method("orbit", &Cow::orbit);
	register_method("create_circle", &Cow::create_circle);
	register_method("timer_timeout", &Cow::timer_timeout);
}

void Cow::_init() {
	// initialize vars
	path = PoolVector3Array();
	path_node = 0;
	speed = 5;
	state = ORBIT;
	radius = 5;
}

void Cow::_ready() {
	// init stuff involving nodes
	nav = Object::cast_to<Navigation>(get_parent());
	player = Object::cast_to<Player>(get_node("/root/Main/Player"));
	milk = Object::cast_to<Spatial>(get_node("/root/Main/store2/IngredientHolder4/milk")); //TODO
	create_circle(milk->get_global_transform().origin); //TODO
	// scan for player
	Variant v = get_node("Area");
	Area *a = Object::cast_to<Area>(v);
	if (a == nullptr) {
		return;
	}
	// attach area_entered signal to scan_for_player callback
	a->connect("body_entered", this, "scan_for_player");
}

// control behavior depending on state
void Cow::_physics_process(float delta) {
	// process
	if (state == REST) {
		move_and_slide(Vector3(0, 0, 0), Vector3::UP);
	} else if (state == ORBIT) {
		orbit();
	} else if (state == BLOCK) {
		move_to_block();
	}
}

// movement along path to halfway between player and balloon
void Cow::move_to_block() {
	if (path_node < path.size()) {
		Vector3 node = path[path_node];
		Vector3 lastNode = path[path.size() - 1];
		Vector3 dir = (node - get_global_transform().origin);
		Vector3 facing = (lastNode - get_global_transform().origin);
	
		if (dir.length() < 1) {
			path_node += 1;
		} else {
			move_and_slide(dir.normalized() * speed, Vector3::UP);
			path_node += 1;
		}

		look_at(player->get_global_transform().origin, Vector3::UP);
	} else {
		calc_block();
	}
}

// calculate halfway point between player and balloon &set path
void Cow::calc_block() {
	// check milk ingredient
	if (!is_milk()) {
		state = REST;
		return;
	}
	Vector3 playerPos = player->get_global_transform().origin;
	Vector3 ingredientPos = milk->get_global_transform().origin;
	Vector3 dif = playerPos - ingredientPos;
	// if player move far enough away, go back to orbiting
	if (dif.length() > radius * 3) {
		create_circle(milk->get_global_transform().origin);
		state = ORBIT;
		path_node = 0;
		return;
	}
	// go halfway between balloon and player
	Vector3 goal = ingredientPos + dif / 2;
	block_path(goal); //calculate path
	path_node = 0;
}

// check if object entered field of vision is player
void Cow::scan_for_player(Node *obj) {
	if (state != ORBIT)
		return;
	Player *tempPlayer = Object::cast_to<Player>(obj);
	if (tempPlayer == nullptr)
		return;
	// check the player is in the field of view
	Vector3 v1 = path[path_node % (path.size() - 1) + 1] - get_global_transform().origin;
	Vector3 v2 = tempPlayer->get_global_transform().origin - get_global_transform().origin;
	if (v1.dot(v2) > 0.5) {
		state = BLOCK;
	}
}

// orbit the milk
void Cow::orbit() {
	if (path_node < path.size()) {
		// orbit milk
		Vector3 node = path[path_node];
		Vector3 dir = (node - get_global_transform().origin);
		if (dir.length() <= 0.05) {
			path_node += 1;
		} else {
			move_and_slide(dir.normalized() * speed, Vector3::UP);
			look_at(path[path_node % (path.size() - 1) + 1], Vector3::UP);
		}
	} else {
		path_node = 0;
	}
}

// create straight line path to block the player
void Cow::block_path(Vector3 target) {
	path = PoolVector3Array();
	Vector3 mypos = get_global_transform().origin;
	Vector3 dist = target - mypos;
	int64_t steps = 10;
	for (int i = 1; i <= steps; i++) {
		float x = mypos.x + i * (dist.x / steps);
		float y = mypos.y + i * (dist.y / steps);
		float z = mypos.z + i * (dist.z / steps);
		path.append(Vector3(x, y, z));
	}
}

// create path to milk & to start circle
void Cow::create_circle(Vector3 center) {
	path = PoolVector3Array();
	for (float theta = 0.0; theta < 360; theta += 3) {
		float x = center[0] + radius * cos(Math::deg2rad(theta));
		float z = center[2] + radius * sin(Math::deg2rad(theta));
		path.append(Vector3(x, center[1], z));
	}
}

// periodically check if the milk we're guarding has been collected
void Cow::timer_timeout() {
	if (!is_milk()) {
		state = REST;
	}
}

// check if ingredient collected or not
bool Cow::is_milk() {
	if (milk->is_visible()) {
		return true;
	}
	return false;
}
