#pragma once

#include <Godot.hpp>
#include <Input.hpp>
#include "state.h"
#include "player.h"

namespace godot {
    class AirState : public State {
        GODOT_CLASS(AirState, State)

        private:
            Player* player; // set in enter()

        public:
            AirState();
            ~AirState();

            void _init();
            void enter(State* oldState);
            void exit(State* newState);
            void update(float delta) override;
            bool check_transitions();
    };
}