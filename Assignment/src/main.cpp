// NEW CODE FOR PROJECT
#include "main.h"

#include "player.h"
#include "ingredientholder.h"

#include <Input.hpp>
#include <PackedScene.hpp>
#include <ResourceLoader.hpp>
#include <SceneTree.hpp>
#include <Viewport.hpp>
#include <TextureRect.hpp>
#include <Texture.hpp>
#include <ImageTexture.hpp>

using namespace godot;

Main::Main() {}

Main::~Main() {}

void Main::_register_methods() {
	register_method("_ready", &Main::_ready);
	register_method("_process", &Main::_process);
	register_method("_ingredient_collision", &Main::_ingredient_collision);
	register_signal<Main>((char *)"_ingredient_clue_signal", "ingredient_clue", GODOT_VARIANT_TYPE_STRING);
	register_signal<Main>((char *)"_leave_scene_signal", "leave_scene", GODOT_VARIANT_TYPE_STRING);
}

void Main::_init() {
	// seed random to generate different dialogue choices
	srand(time(0));
}

void Main::_ready() {
	// set up Main
	recipes = Dictionary();
	keys = Array();
	recipe_indices = Array();

	num_store = 0;
	recipe_index = -1;
	end_of_game = false;
	
	intro = Object::cast_to<Intro>(get_node("Intro"));
	if (intro == nullptr) {
		Godot::print("!!! Main has no Intro/instructions");
		return;
	}
	
	hud = Object::cast_to<HUD>(get_node("HUD"));
    if (hud == nullptr) {
        Godot::print("!!! Main has no HUD");
		return;
    }

	transition = Object::cast_to<Control>(get_node("Transition"));
	if (transition == nullptr) {
        Godot::print("!!! Main has no Transition");
		return;
    }
	transition->set_visible(false);

	// Set up game
	Parser::create_dictionary(recipes, keys, "res://Assets/recipes3.csv.txt");
	pick_recipe();

	Array all_recipes = recipes["recipe"];
	String recipe = all_recipes[recipe_index];
	intro->_display_message(recipe);
}

void Main::_process(float delta){
	Input* input = Input::get_singleton();
    if (input->is_action_pressed("restart") && end_of_game) {
        get_tree()->reload_current_scene();
		intro->set_visible(true);
    }
	if (input->is_action_pressed("intro_esc") && intro->is_visible()) {
		intro_scene();
		add_scene();
		intro->set_visible(false);
	}
}

void Main::intro_scene(){
	transition_scene();
	num_store +=1;
}

void Main::end_scene(){
	transition_scene();
	end_of_game = true;
	Array attempted_recipes = Array();
	Array all_recipes = recipes["recipe"];

	//get names of attempted recipes
	for (int i = 0; i < recipe_indices.size(); i++) {
		int index = recipe_indices[i];
		attempted_recipes.append(all_recipes[index]);
	}
	hud->_display_message(attempted_recipes);
}

void Main::transition_scene(){
	Ref<Image> image;
    image.instance();

	// load store scenes, otherwise do background
	if (num_store > 0 && num_store < keys.size()){
		image->load("res://Assets/materials/store"+ String::num_int64(num_store)+".jpg");
	} else {
		image->load("res://Assets/materials/background.png");
	}
    
    ImageTexture* imTex = ImageTexture::_new();
    imTex->create_from_image(image);
    
	// set background
	TextureRect* tr = Object::cast_to<TextureRect>(transition->get_node("TextureRect"));
	tr->set_texture(imTex);
	transition->set_visible(true);
}


void Main::add_scene() {
	transition_scene();
	
	// get scene
	Ref<PackedScene> sceneToAdd = ResourceLoader::get_singleton()->load("res://Assets/scenes/store"+ String::num_int64(num_store) +".tscn");
	Node* newNode = sceneToAdd->instance();

	// add scene to main
	Node* main = get_tree()->get_root()->get_node("Main");
	main->add_child(newNode);

	// hide ingredients that don't need to be shown in store
	hide_ingredients();
}

void Main::pick_recipe() {
	// pick random recipe
	Array available = recipes["available"];
	if (!available.has(recipe_index)) {
		int temp = rand() % available.size();
		recipe_index = available[temp];
		recipe_indices.append(recipe_index);
	}
}

void Main::hide_ingredients(){
	Array nodes = get_tree()->get_nodes_in_group("store"+ String::num_int64(num_store));
	Array ingredients = recipes["ingredient"+String::num_int64(num_store)];
	Array available = recipes["available"];
	Array ingredientNames = Array();

	//update recipe we're making & send clue
	pick_recipe();
	String ingredient_clue = ingredients[recipe_index];
	emit_signal("_ingredient_clue_signal", ingredient_clue);

	// get avaiable ingredient names
	for (int i = 0; i < available.size(); i++){
		ingredientNames.append(ingredients[available[i]]);
	}

	// loop through stores ingredients
	for (int i = 0; i < nodes.size(); i++) {
		IngredientHolder* food = Object::cast_to<IngredientHolder>(nodes[i]);
		String name = food->get_child(food->get_child_count()-1)->get_name();
		food->set_nameTag(name);

		// connect if ingredient is part of available list, hide if not shown
		if (ingredientNames.has(name)) {
			food->connect("_ingredient_signal", this, "_ingredient_collision");
		} else {
			food->set_visible(false);
		}
	}
}

void Main::_ingredient_collision(String ingredient) {
	update_recipes(ingredient);
}

void Main::update_recipes(String ingredient) {
	Array available = recipes["available"];
	String key = keys[num_store];
	int i = 0;
	// loop through available list
	while (i < available.size()){
		int pos = available[i];
		Array ingredients = recipes[key];

		// remove from recipes if not ingredient chosen
		if (ingredients[pos] != ingredient){
			String x = ingredients[pos];
			available.remove(i);
		} else {
			i++;
		}
	}

	recipes["available"] = available;

	next_store();
}

void Main::next_store(){
	// hide previous level
	NodePath storename = "Main/store" + String::num_int64(num_store);
	Node* store = get_tree()->get_root()->get_node(storename);
	if (store != nullptr){
		store->queue_free();
	}

	// move onto next store
	num_store += 1;
	if (num_store < keys.size()){
		add_scene();
	} else { // end of game
		emit_signal("_leave_scene_signal", "ending");
		end_scene();
	}
}

void Main::print_dictionary() {
	// print dictionary (HERE FOR DEBUGGING)
	for (int i = 0; i < keys.size(); i++){
		Godot::print(keys[i]);
		Godot::print(recipes[keys[i]]);
	}
	Array available = recipes["available"];
	for (int i = 0; i < available.size(); i++){
		Godot::print(String::num_int64(available[i]));
	}
}
