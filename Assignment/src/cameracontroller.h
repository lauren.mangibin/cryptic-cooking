#ifndef CAMERACONTROLLER
#define CAMERACONTROLLER

#include <Godot.hpp>
#include <SpringArm.hpp>
#include <KinematicBody.hpp>

namespace godot {

    class CameraController : public SpringArm {
        GODOT_CLASS(CameraController, SpringArm)

    private:
        float panSpeed;
        float panMaxSpeed;
        float panAcceleration;

    public:
        static void _register_methods();

        CameraController();
        ~CameraController();

        void _init(); // our initializer called by Godot
		
        void _physics_process(float delta);
    };

}

#endif