// NEW CODE FOR PROJECT
#ifndef Intro_H
#define Intro_H

#include <Control.hpp>
#include <Godot.hpp>
#include <Label.hpp>

namespace godot {

class Intro : public Control {
	GODOT_CLASS(Intro, Control)

	private:
		Label *recipe;

	public:
		static void _register_methods();

		Intro();
		~Intro();

		void _init();
		void _ready();

		void _display_message(String recipe);
	};

} // namespace godot

#endif
