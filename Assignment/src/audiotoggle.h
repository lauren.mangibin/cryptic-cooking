#pragma once

#include <Godot.hpp>
#include <CheckBox.hpp>

#include "player.h"

namespace godot {
    class AudioToggle : public CheckBox {
        GODOT_CLASS(AudioToggle, CheckBox)

        private:

        public:
            static void _register_methods();

            String targetBus;

            AudioToggle();
            ~AudioToggle();

            void _init();
            void _ready();
            void _on_toggled(bool isOn);
    };
}