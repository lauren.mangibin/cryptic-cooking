#pragma once

#include <Godot.hpp>
#include "cameracontroller.h"
#include "player.h"

namespace godot {
    class Movement {
        private:
        public:
            Movement();
            ~Movement();

            static void move(float delta, Player* player);
            static void rotate(float delta, Vector3 direction, Player* player, bool shouldSnapRotation);
            static Vector3 getInputDirection(CameraController* camera);
            static float signed_angle_to(Vector3 u, Vector3 v, Vector3 axis);
    };
}